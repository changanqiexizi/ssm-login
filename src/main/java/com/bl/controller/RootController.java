package com.bl.controller;

import com.bl.bean.Root;
import com.bl.dao.RootDAO;
import com.bl.service.RootService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/rootController")
public class RootController {

    @Resource
    private RootService rootService;

    @RequestMapping("/login")
    public String login(String username, String password, HttpServletRequest req) {
        Root root = rootService.checkLogin(username, password);
        if (root == null) {
            req.setAttribute("tips", "false");
            return "/login.jsp";
        } else {
            req.getSession().setAttribute("root", "root");
            return "redirect:/index.jsp";
        }

    }

}
