package com.bl.service.Impl;

import com.bl.bean.Root;
import com.bl.dao.RootDAO;
import com.bl.service.RootService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class RootServiceImpl implements RootService {

    @Resource
    private RootDAO rootDAO;

    public Root checkLogin(String a, String b) {

        Root query = rootDAO.query(a);

        if (query.getPassword().equals(b)) {
            return query;
        } else {
            return null;
        }
    }
}
