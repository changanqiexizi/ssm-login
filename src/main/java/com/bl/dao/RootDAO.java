package com.bl.dao;

import com.bl.bean.Root;

public interface RootDAO {

    public Root query(String name);
}
