package com.bl.service;

import com.bl.service.Impl.RootServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring-context.xml", "classpath:spring-mvc.xml", "classpath:spring-mybatis.xml"})
public class RootServiceTest {

    @Resource
    private RootServiceImpl rootService;

    @Test
    public void checkLogin() {
        System.out.println(rootService.checkLogin("asd", "123"));
    }
}